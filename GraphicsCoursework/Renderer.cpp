#include "Renderer.h"
Renderer::Renderer(Window &parent) : OGLRenderer(parent) {

	camera = new Camera(-40, 270, Vector3(-2100, 3300, 2000));

	//camera = new Camera(-40, 270, Vector3(-0, 0, 0));

	// stuff for 3 objects. Remember that ordering matters so you should generate transparent object last! OR sort by their z value.
	meshes[2] = Mesh::GenerateTriangle();
	meshes[1] = Mesh::GenerateQuad();
	meshes[0] = Mesh::GenerateQuad();
	meshes[2]->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"stainedglass.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	meshes[1]->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"brick.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));
	meshes[0]->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"brick.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0));

	// terrain
	heightMap = new HeightMap(TEXTUREDIR"terrain.raw");
	heightMap->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Barren Reds.JPG", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	//bump map
	heightMap->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.JPG", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	//post processsing
	processShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"processfrag.glsl"); 
	sceneShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"TexturedFragment.glsl");
	currentShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"TexturedFragment.glsl");
	//hellShader = new Shader(SHADERDIR"TexturedVertex.glsl", SHADERDIR"TexturedFragment.glsl");
	quad = Mesh::GenerateQuad(); // for storing the screen sized texture?

	glGenTextures(1, &bufferDepthTex);
	glBindTexture(GL_TEXTURE_2D, bufferDepthTex);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH24_STENCIL8, width, height, 0, GL_DEPTH_STENCIL, GL_UNSIGNED_INT_24_8, NULL);

	for (int i = 0; i < 2; ++i) {
		glGenTextures(1, &bufferColourTex[i]);
		glBindTexture(GL_TEXTURE_2D, bufferColourTex[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	}
	glGenFramebuffers(1, &bufferFBO); //We�ll render the scene into this
	glGenFramebuffers(1, &processFBO);//And do post processing in this 
	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, bufferDepthTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_STENCIL_ATTACHMENT, GL_TEXTURE_2D, bufferDepthTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferColourTex[0], 0);  //We can check FBO attachment success using this command!

	/*if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE || !sceneDepthTex || !sceneColourTex[0]) {
		return;
	}*/

	glBindFramebuffer(GL_FRAMEBUFFER, 0);


	if (!heightMap->GetTexture() || !heightMap->GetBumpMap()) { return;} 
	SetTextureRepeating(heightMap->GetTexture(), true);
	SetTextureRepeating(heightMap->GetBumpMap(), true);

	if (!meshes[0]->GetTexture() || !meshes[1]->GetTexture() || !meshes[2]->GetTexture()) {return;}

	if (!currentShader->LinkProgram() || !processShader->LinkProgram() || !sceneShader->LinkProgram()) {return;}

	//hell knight
	hellData = new MD5FileData(MESHDIR"hellknight.md5mesh");
	hellNode = new MD5Node(*hellData);


	hellData->AddAnim(MESHDIR"idle2.md5anim");
	hellNode->PlayAnim(MESHDIR"idle2.md5anim");

	// cube person
	currentShaderCube = new Shader(SHADERDIR"SceneVertex.glsl", SHADERDIR"SceneFragment.glsl");
	CubeRobot::CreateCube();

	if (!currentShaderCube->LinkProgram()) {return;}

	root = new SceneNode();
	root->AddChild(new CubeRobot());

	for (int i = 1; i < 11; i++) {
		SceneNode* c = new CubeRobot();
		c->SetTransform(Matrix4::Translation(Vector3( i * 500 , 0, 0)) * Matrix4::Scale(Vector3(10,10,10)));
		root->AddChild(c);
	}

	// lighting
	currentShaderLight = new Shader(SHADERDIR"PerPixelVertex.glsl", SHADERDIR"PerPixelFragment.glsl");

	if (!currentShaderLight->LinkProgram()) {return;}

	//light = new Light(Vector3((RAW_HEIGHT*HEIGHTMAP_X / 2.0f), 500.0f, (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f)), Vector4(0.850, 0.156, 0.149, 1), (RAW_WIDTH*HEIGHTMAP_X) / 2.0f);
	light = new Light(Vector3((RAW_HEIGHT*HEIGHTMAP_X / 2.0f), 500.0f,  (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f)),  Vector4(0.9f, 0.9f, 1.0f, 1),  (RAW_WIDTH*HEIGHTMAP_X) / 2.0f);

	// SkyBox
	reflectShader = new Shader(SHADERDIR"PerPixelVertex.glsl", SHADERDIR "reflectFragment.glsl");
	skyboxShader = new Shader(SHADERDIR"skyboxVertex.glsl", SHADERDIR "skyboxFragment.glsl");
	quad2 = Mesh::GenerateQuad();
	// try and use light as the lightShader
	if (!reflectShader->LinkProgram() || !skyboxShader->LinkProgram()) {
		return;
	}

	quad2->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"water.TGA", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	cubeMap = SOIL_load_OGL_cubemap(TEXTUREDIR"rusted_west.jpg", TEXTUREDIR"rusted_east.jpg", TEXTUREDIR"rusted_up.jpg", TEXTUREDIR"rusted_down.jpg", TEXTUREDIR"rusted_south.jpg", TEXTUREDIR"rusted_north.jpg", SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0); 
	if (!cubeMap || !quad2->GetTexture() || !heightMap->GetTexture() || !heightMap->GetBumpMap()) {
		return;
	}

	SetTextureRepeating(quad2->GetTexture(), true);
	waterRotate = 0.0f;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); 
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);


	// Variables
	init = true;

	filtering = true;
	repeating = false;

	usingDepth = false;
	usingAlpha = false;
	blendMode = 0;
	modifyObject = true;

	//glEnable(GL_CULL_FACE);
	//glCullFace(GL_BACK);

	//SwitchToOrthographic();

	SwitchToPerspective();
	ToggleDepth();
} 

Renderer::~Renderer(void) { 
	delete triangle;
	delete meshes[0];
	delete meshes[1];
	delete meshes[2];
	CubeRobot::DeleteCube();
	delete heightMap;
	delete camera;
	delete hellData;
	delete hellNode;
	delete light;
	delete processShader;
	currentShader = NULL;
	delete quad;
	delete sceneShader;
	glDeleteTextures(2, bufferColourTex); 
	glDeleteTextures(1, &bufferDepthTex); 
	glDeleteFramebuffers(1, &bufferFBO); 
	glDeleteFramebuffers(1, &processFBO);
	delete quad2;
	delete reflectShader;
	delete skyboxShader;


}

void Renderer::RenderScene() {

	if (blur){
		glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
		SetCurrentShader(sceneShader);
		projMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / (float)height, 45.0f);
		UpdateShaderMatrices(sceneShader);
	}

//	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	//modelMatrix.ToZero();
	//textureMatrix.ToZero();

	// skybox
	DrawSkybox();

	

	// for the 3 objects
	//glUseProgram(currentShader->GetProgram());
	//UpdateShaderMatrices(currentShader);

	//for (int i = 0; i < 3; i++) {
	//	Vector3 tempPos = positions[i];
	//	tempPos.z += (i*500.0f); 
	//	tempPos.x -= (i*100.0f); 
	//	tempPos.y -= (i*100.0f);
	//  modelMatrix = Matrix4::Translation(tempPos) * Matrix4::Rotation(rotation, Vector3(0, 1, 0)) * Matrix4::Scale(Vector3(scale, scale, scale));

	//	glActiveTexture(GL_TEXTURE0);
	//	glBindTexture(GL_TEXTURE_2D, 0);
	//	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&modelMatrix); 
	//	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0); // for texture unit	
	//	meshes[i]->Draw();
	//}
	
	// hell knight
	glUseProgram(sceneShader->GetProgram()); // for some reason currentShader changes the fragment shader to the cubetex fragment shader, so I just use another one 
	modelMatrix = Matrix4::Scale(Vector3(10, 10, 10)) * Matrix4::Translation(Vector3(200, 0, 300));
	UpdateShaderMatrices(sceneShader);
	glUniform1i(glGetUniformLocation(sceneShader->GetProgram(), "diffuseTex"), 0);
	hellNode->Draw(*this);
	

	// for the robot
	glUseProgram(currentShaderCube->GetProgram());
	UpdateShaderMatrices(currentShaderCube);
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "modelMatrix"), 1, false, (float*)&modelMatrix); // I could probably put this in the UpdateShaderMatrcies function
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	DrawNode(root);

	
	// light and terrain
	glUseProgram(currentShaderLight->GetProgram());
	glUniform1i(glGetUniformLocation(currentShaderLight->GetProgram(), "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShaderLight->GetProgram(), "bumpTex"), 1);
	glUniform3fv(glGetUniformLocation(currentShaderLight->GetProgram(), "cameraPos"), 1, (float*)&camera->GetPosition());

	modelMatrix.ToIdentity();
	UpdateShaderMatrices(currentShaderLight);
	SetShaderLight(*light, currentShaderLight);
	heightMap->Draw();

	

	// water
	DrawWater();

	if (blur) {
		DrawPostProcess();
		PresentScene();
	}
	glUseProgram(0); 
	SwapBuffers(); 

}

void Renderer::SwitchToPerspective() {
	projMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / (float)height, 45.0f);
}

void Renderer::SwitchToOrthographic() {
	projMatrix = Matrix4::Orthographic(-1.0f, 10000.0f, width / 2.0f, -width / 2.0f, height / 2.0f, -height / 2.0f);
}

void Renderer::UpdateTextureMatrix(float value)
{
	Matrix4 pushPos = Matrix4::Translation(Vector3(0.5f, 0.5f, 0));
	Matrix4 popPos = Matrix4::Translation(Vector3(-0.5f, -0.5f, 0)); 
	Matrix4 rotation = Matrix4::Rotation(value, Vector3(0, 0, 1)); 
	textureMatrix = pushPos * rotation * popPos;
}

void Renderer::ToggleRepeating()
{
	repeating = !repeating;
	glBindTexture(GL_TEXTURE_2D, triangle->GetTexture()); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, //x axis
		repeating ? GL_REPEAT : GL_CLAMP); 
	glTexParameteri(GL_TEXTURE_2D , GL_TEXTURE_WRAP_T , //y axis
		repeating ? GL_REPEAT : GL_CLAMP);
	glBindTexture(GL_TEXTURE_2D , 0); 
}

void Renderer::ToggleFiltering()
{
	filtering = !filtering;
	glBindTexture(GL_TEXTURE_2D, triangle->GetTexture());
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
		filtering ? GL_LINEAR : GL_NEAREST); 
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, 
		filtering ? GL_LINEAR : GL_NEAREST);
	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::ToggleObject()
{
	modifyObject = !modifyObject;
}

void Renderer::ToggleDepth()
{
	usingDepth = !usingDepth;
	usingDepth ? glEnable(GL_DEPTH_TEST) : glDisable(GL_DEPTH_TEST);
}

void Renderer::ToggleAlphaBlend()
{
	usingAlpha = !usingAlpha;
	usingAlpha ? glEnable(GL_BLEND) : glDisable(GL_BLEND);
}

void Renderer::ToggleBlendMode()
{
	blendMode = (blendMode + 1) % 4;

	switch (blendMode) {
	case(0):glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);break;
	case(1):glBlendFunc(GL_SRC_COLOR, GL_ONE_MINUS_SRC_COLOR);break;
	case(2):glBlendFunc(GL_ONE, GL_ZERO);break;
	case(3):glBlendFunc(GL_SRC_ALPHA, GL_ONE);break;
	};
}

void Renderer::MoveObject(float by)
{
	positions[(int)modifyObject].z += by;
}

void Renderer::UpdateScene(float msec) {
	camera->UpdateCamera(msec);
	viewMatrix = camera->BuildViewMatrix();
	root->Update(msec);
	hellNode->Update(msec);
	waterRotate += msec / 1000.0f;
}

void Renderer::DrawNode(SceneNode*n) {

	if (n->GetMesh()) { 

		Matrix4 transform = n->GetWorldTransform() * Matrix4::Scale(n->GetModelScale()); 

		glUniformMatrix4fv( glGetUniformLocation(currentShaderCube->GetProgram(),  "modelMatrix"), 1, false, (float*)&transform); 
		glUniform4fv(glGetUniformLocation(currentShaderCube->GetProgram(), "nodeColour"), 1, (float*)&n->GetColour()); 
		glUniform1i(glGetUniformLocation(currentShaderCube->GetProgram(), "useTexture"), (int)n->GetMesh()->GetTexture()); 

		n->Draw(*this); 

	} 

	for (vector <SceneNode*>::const_iterator i = n->GetChildIteratorStart(); 
		i != n->GetChildIteratorEnd(); ++i) { 
		DrawNode(*i); 
	} 
}

void Renderer::DrawScene() { 

	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO); 

	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT |  GL_STENCIL_BUFFER_BIT); 

	SetCurrentShader(sceneShader); 
	projMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / (float)height, 45.0f); 

	UpdateShaderMatrices(sceneShader);

	heightMap->Draw(); 
	glEnable(GL_DEPTH_TEST);
	glUseProgram(0); 

	glBindFramebuffer(GL_FRAMEBUFFER, 0); 
}

void Renderer::DrawPostProcess() {
	glBindFramebuffer(GL_FRAMEBUFFER, processFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferColourTex[1], 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	SetCurrentShader(processShader);
	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	viewMatrix.ToIdentity();
	UpdateShaderMatrices(processShader);
	glDisable(GL_DEPTH_TEST);
	glUniform2f(glGetUniformLocation(currentShader->GetProgram(), "pixelSize"), 1.0f / width, 1.0f / height);

	for (int i = 0; i < POST_PASSES; ++i) {
		 glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferColourTex[1], 0); 
		 glUniform1i(glGetUniformLocation(currentShader->GetProgram(),"isVertical"), 0); 
		 quad->SetTexture(bufferColourTex[0]);
		 quad->Draw(); 
		 //Now to swap the colour buffers , and do the second blur pass 
		 glUniform1i(glGetUniformLocation(currentShader ->GetProgram(), "isVertical"), 1);
		 glFramebufferTexture2D(GL_FRAMEBUFFER , GL_COLOR_ATTACHMENT0 , GL_TEXTURE_2D , bufferColourTex[0], 0); 
		 quad ->SetTexture(bufferColourTex [1]); 
		 quad ->Draw(); 
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0); 
	glUseProgram(0); 
	glEnable(GL_DEPTH_TEST);
}

void Renderer::PresentScene() { 
	glBindFramebuffer(GL_FRAMEBUFFER, 0); 
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT); 
	SetCurrentShader(sceneShader); 
	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1); 
	viewMatrix.ToIdentity(); 
	UpdateShaderMatrices(sceneShader); 
	quad->SetTexture(bufferColourTex[0]); 
	quad->Draw(); 
	glUseProgram(0); 
}

void Renderer::DrawSkybox() { 
	glDepthMask(GL_FALSE);
	glDisable(GL_DEPTH_TEST);
	//glUseProgram(skyboxShader->GetProgram());
	SetCurrentShader(skyboxShader); 
	UpdateShaderMatrices(skyboxShader); 
	quad->Draw(); 
	glUseProgram(0); // unbinding the the shader 
	glDepthMask(GL_TRUE); 
	glEnable(GL_DEPTH_TEST);
	//SetCurrentShader(currentShader);
}

void Renderer::DrawWater() {
	SetCurrentShader(reflectShader);
	SetShaderLight(*light,reflectShader); 
	glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&camera->GetPosition());
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(),  "diffuseTex"), 0);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTex"), 2);
	glActiveTexture(GL_TEXTURE2); 
	glBindTexture(GL_TEXTURE_CUBE_MAP, cubeMap); 
	float heightX = (RAW_WIDTH*HEIGHTMAP_X / 2.0f);  
	float heightY = 256 * HEIGHTMAP_Y / 3.0f; 
	float heightZ = (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f); 
	modelMatrixWater= Matrix4::Translation(Vector3(heightX, heightY, heightZ)) * Matrix4::Scale(Vector3(heightX, 1, heightZ)) * Matrix4::Rotation(90, Vector3(1.0f, 0.0f, 0.0f)); 
	textureMatrixWater = Matrix4::Scale(Vector3(10.0f, 10.0f, 10.0f)) * Matrix4::Rotation(waterRotate, Vector3(0.0f, 0.0f, 1.0f));
	UpdateShaderMatrices(reflectShader); //not using UpdateShaderMatrices so i can have consisten matricies
	glUniformMatrix4fv(glGetUniformLocation(reflectShader->GetProgram(), "modelMatrix"), 1, false, (float*)&modelMatrixWater);
	glUniformMatrix4fv(glGetUniformLocation(reflectShader->GetProgram(), "textureMatrix"), 1, false, (float*)&textureMatrixWater);
	glUniformMatrix4fv(glGetUniformLocation(reflectShader->GetProgram(), "viewMatrix"), 1, false, (float*)&viewMatrix);
	glUniformMatrix4fv(glGetUniformLocation(reflectShader->GetProgram(), "projMatrix"), 1, false, (float*)&projMatrix);


	quad2->Draw(); 
	glUseProgram(0);
}