#pragma once
#include "..\nclgl\OGLRenderer.h"

// added a normal buffer
enum MeshBuffer {
	VERTEX_BUFFER, COLOUR_BUFFER, TEXTURE_BUFFER, NORMAL_BUFFER, TANGENT_BUFFER, INDEX_BUFFER, MAX_BUFFER
};
class Mesh {
public:
	Mesh(void);
	~Mesh(void);

	virtual void Draw();
	static Mesh* GenerateTriangle();
	static Mesh* GenerateQuad();

	void SetTexture(GLuint tex) { texture = tex; }
	GLuint GetTexture() { return texture; }

	void GenerateNormals();

	void SetBumpMap(GLuint tex) { bumpTexture = tex; }

	GLuint GetBumpMap() { return bumpTexture; }


protected:

	void BufferData();

	// for textures and texture coordinates
	GLuint texture;
	Vector2* textureCoords;

	GLuint arrayObject;
	GLuint bufferObject[MAX_BUFFER];
	GLuint numVertices;
	GLuint type;

	// terrain information
	GLuint numIndices;
	unsigned int* indices;

	Vector3* vertices;
	Vector4* colours;

	// normal of triangle/verticies or fragment
	Vector3* normals;
	//Vector3* normalss[4];

	// bump mapping
	void GenerateTangents();
	Vector3 GenerateTangent(const Vector3 &a, const Vector3 &b, const Vector3 &c, const Vector2 &ta, const Vector2 &tb, const Vector2 &tc);

	Vector3* tangents;
	//Vector3* tangentss[4];
	GLuint bumpTexture;

	//normals and tangents array
};