#pragma once
#include "..\nclgl\OGLRenderer.h"
#include "Camera.h"
#include "SceneNode.h"
#include "CubeRobot.h"
#include "..\nclgl\HeightMap.h"
#include "..\nclgl\MD5Mesh.h"
#include "..\nclgl\MD5Node.h"
#include "..\nclgl\MD5Anim.h"

#define POST_PASSES 2 // the number of time for the ping pong(blur) effect

class Renderer : public OGLRenderer {
public:
	Renderer(Window &parent);
	virtual ~Renderer(void);
	virtual void RenderScene();
	
	void SwitchToPerspective();
	void SwitchToOrthographic();

	inline void SetScale(float s) { scale = s; } 
	inline void SetRotation(float r) { rotation = r; }
	inline void SetPosition(Vector3 p) { position = p; }

	// texture stuff
	void UpdateTextureMatrix(float rotation); // text coord transfomation
	// ways that textures are presented
	void ToggleRepeating();
	void ToggleFiltering();

	// toggles for object transprancy
	void ToggleObject();
	void ToggleDepth();
	void ToggleAlphaBlend();
	void ToggleBlendMode();
	void MoveObject(float by);

	// post processing stuff
	void PresentScene();
	void DrawPostProcess(); 
	void DrawScene();
	void EnableBlur() {blur = !blur; SwitchToPerspective();}

	// SkyBox stuff
	//void DrawHeightmap();
	void DrawWater();
	void DrawSkybox();



	virtual void UpdateScene(float msec);

protected:
	Mesh* triangle;
	Camera* camera;

	float scale;
	float rotation;
	Vector3 position;
	bool filtering;
	bool repeating;

	// information for  transprancy
	GLuint textures[3];
	Mesh* meshes[3];
	Vector3 positions[3]; 

	//oglRenderer has Matrix4
	/*Matrix4 textureMatrix; 
	Matrix4 modelMatrix; 
	Matrix4 projMatrix;
	Matrix4 viewMatrix; */

	bool modifyObject;
	bool usingDepth; 
	bool usingAlpha; 
	int blendMode;

	// tutorial 6 scene node
	void DrawNode(SceneNode*n);
	SceneNode* root;

	// tutorial 8 terrian
	HeightMap* heightMap;

	//tutorial 9 hell knight
	MD5FileData* hellData;
	MD5Node* hellNode;

	//tutorial 11 lighting
	Light* light;
	//HeightMap* lightHeightMap;

	 // for the hellknight to keep moving
	int counter =0;
	Matrix4 hellKnightTrix;
	bool initial = false;

	// post processing 
	Mesh* quad;
	Shader* sceneShader;
	Shader* processShader;
	GLuint bufferFBO;
	GLuint processFBO;
	GLuint bufferColourTex[2]; 
	GLuint bufferDepthTex;
	bool blur = false;

	// skybox stuff
	Shader* lightShader;
	Shader* reflectShader;
	Shader* skyboxShader;
	Shader* hellShader;

	Mesh* quad2;
	Light* reflectLight;

	GLuint cubeMap;
	float waterRotate;
	Matrix4 modelMatrixWater;
	Matrix4 textureMatrixWater;
};
