#pragma comment(lib, "nclgl.lib") // changed from framework.lib
//#include "..\nclgl\OGLRenderer.h"
//#include "..\nclgl\Keyboard.h"

#include "../nclgl/Window.h"
#include "Renderer.h"

int main() {
	Window w("My First OpenGL 3 Triangle!", 1440 , 900, false);
	if(!w.HasInitialised()) {
		return -1;
	} 
	Renderer renderer(w); 
	if(!renderer.HasInitialised()) { 
		return -1; 
	} 
	
	float scale = 100.0f;
	float rotation = 0.0f;
	Vector3 position(0, 0, -1500.0f); 

	// locking mouse to screen
	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);
	//glEnable(GL_DEPTH_TEST); // why would GL_DEPTH_TEST BREAK EVERYTHNIG

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {

		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_1))
			renderer.EnableBlur(); 

		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_2)) 
			renderer.ToggleDepth();

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_3))
			renderer.ToggleFiltering();

		//if (Window::GetKeyboard()->KeyDown(KEYBOARD_4))
		//	renderer.ToggleRepeating();

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_5))
			renderer.ToggleDepth();

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_6))
			renderer.ToggleAlphaBlend();

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_7))
			renderer.ToggleBlendMode();

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_PLUS))
			++scale;

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_MINUS))
			--scale; 

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_LEFT)) {
			++rotation;
			//renderer.UpdateTextureMatrix(rotation);
		}

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_RIGHT)) {
			--rotation;
			//renderer.UpdateTextureMatrix(rotation);
		}

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_K)) 
			position.y -= 1.0f;

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_I))
			position.y += 1.0f; 

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_J)) 
			position.x -= 1.0f;

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_L))  
			position.x += 1.0f; 

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_O)) 
			position.z -= 1.0f; 

		if (Window::GetKeyboard()->KeyDown(KEYBOARD_P)) 
			position.z += 1.0f; 

		// you need this omegalul
		renderer.SetRotation(rotation);
		renderer.SetScale(scale); 
		renderer.SetPosition(position);
	
		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();

	} 

	
	return 0; 
}
