#version 150 core 

uniform sampler2D diffuseTex;

in Vertex{
 vec2 texCoord;
} IN;

out vec4 fragColour;

void main(void) {
	vec4 col = texture(diffuseTex, IN.texCoord);
	//col.a = 0.5;
	fragColour = col;
}
